import utctobeijing from "@/utils/utctobeijing.js"
export const $loadOrder = function() { // 发起请求
	let skip = this.page * 6

	var url = `/1.1/classes/order?where=${JSON.stringify(this.state)}&limit=6&skip=${skip}&order=-createdAt`
	if(this.state.confirm == '99'){
		url = `/1.1/classes/order?&limit=6&skip=${skip}&order=-createdAt`
	}

	this.$get(url).then(res => {
		uni.stopPullDownRefresh()
		let { results } = res
		 // utc转北京时间
		results.forEach(el => {
        el.createdAt = utctobeijing(el.createdAt)
      })
			if (results.length) { // 下拉返回数据不为空时，追加数据
				this.page++
				this.orderList = [...this.orderList, ...res.results]
				return
			}
			uni.showToast({ // 下拉返回数据为空时，提示用户
				title: '这回真没了',
				icon: 'none'
			})
	})
}
