export const $loadData = function(bool) { // 发起请求
	let skip = this.page * 8
	let wh = JSON.stringify(this.condition)
	let url = `/1.1/classes/cake?where=${wh}&limit=8&skip=${skip}`
	this.$get(url).then(res => {
		uni.stopPullDownRefresh()
		let { results } = res
		if (bool) { // 判断是否是侧边栏的请求，如果是考虑展示空界面
			if (!results.length) {
				this.showGoods = false
				this.showempty = true
			} else {
				this.showGoods = true
				this.showempty = false
			}
			if (results.length) { // 下拉返回数据不为空时，追加数据
				this.page++
				this.glist = [...this.glist, ...res.results]
				return
			}
			uni.showToast({ // 下拉返回数据为空时，提示用户
				title: '这回真没了',
				icon: 'none'
			})
		} else {
			if (results.length) { // 下拉返回数据不为空时，追加数据
				this.page++
				this.glist = [...this.glist, ...res.results]
				return
			}
			uni.showToast({ // 下拉返回数据为空时，提示用户
				title: '这回真没了',
				icon: 'none'
			})
		}
	})
}
