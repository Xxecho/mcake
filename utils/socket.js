import io from '@wu-xj/uni-socket.io'
import store from '@/store/index.js';
import algorithm from "@/utils/algorithm.js"

const socket = io('http://1.12.49.243:8089', {
  query: {},
  transports: ['websocket', 'polling'],
  timeout: 5000,
})



socket.on('connect', () => {
  // 主动向服务器发送数据
  // socket.emit('pushMsg', '你好医生，我明天需要来就医')
  socket.on('pushMsg', (data) => {
    // console.log(data) // 你好，我是服务端 
    let id = algorithm()
    store.commit('chat/chatAddManage', [id,data])
  })

  socket.on('error', (msg) => {
    console.log('ws error', msg)
  })
})

export default socket
