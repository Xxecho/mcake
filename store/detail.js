export default{
	state(){
		return{
			goodsDetail:{}, // 商品列表查询条件对象
			nav:0,// 筛选底部导航栏的样式
			goodsNav:0,// 筛选分类界面顶部导航栏的样式
		}
	},
	mutations:{
		handleNav(state,index){
			state.nav = index
		}
	}
}