const state = {
  cartList:[],
  address:{},
  allInfo:{},
  conf:'', //保存个人中心传过来的confirm

}

const mutations = {
  add(state,data){
    console.log(data);
    state.cartList = data[0]
    state.address = data[1]
    state.allInfo = data[2]
  },
  changeConf(state,data){
    // console.log(data);
    state.conf = data
  }
}

const actions = {
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
