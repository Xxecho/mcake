const state = {
  chat:[
    // {source:'user', message:'你好'},
    // {source:'manage', message:'请问有什么可以帮到您'},
    // {source:'user', message:'最近这个蛋糕有活动吗'},
    // {source:'manage', message:'有的'},
    // {source:'manage', message:'满一百减一百满一百减一百满一百减一百满一百减一百满一百减一百满一百减一百'},
    // {source:'manage', message:'满一百减一百'},
    // {source:'manage', message:'满一百减一百'},
    // {source:'manage', message:'满一百减一百'},
    // {source:'manage', message:'满一百减一百'},
    // {source:'manage', message:'满一百减一百'},
    // {source:'user', message:'最近这个蛋糕有活动吗?'},
  ],
}

const mutations = {
  chatAddUser(state, msg) {
    state.chat.push({id:msg[0], source: 'user', message: msg[1] })
  },
  chatAddManage(state, msg) {
    state.chat.push({id:msg[0], source: 'manage', message: msg[1] })
  },
}

const actions = {
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
