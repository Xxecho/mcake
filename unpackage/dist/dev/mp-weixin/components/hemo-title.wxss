@charset "UTF-8";
/**
 * 这里是uni-app内置的常用样式变量
 *
 * uni-app 官方扩展插件及插件市场（https://ext.dcloud.net.cn）上很多三方插件均使用了这些样式变量
 * 如果你是插件开发者，建议你使用scss预处理，并在插件代码中直接使用这些变量（无需 import 这个文件），方便用户通过搭积木的方式开发整体风格一致的App
 *
 */
/**
 * 如果你是App开发者（插件使用者），你可以通过修改这些变量来定制自己的插件主题，实现自定义主题功能
 *
 * 如果你的项目同样使用了scss预处理，你也可以直接在你的 scss 代码中使用如下变量，同时无需 import 这个文件
 */
/*
  ColorUi for uniApp  v2.1.6 | by 文晓港 2019-05-31 10:44:24
  仅供学习交流，如作它用所承受的法律责任一概与作者无关  
  
  *使用ColorUi开发扩展与插件时，请注明基于ColorUi开发 
  
  （QQ交流群：240787041）
*/
/* ==================
        初始化
 ==================== */
page {
	background-color: #f1f1f1;
	font-size: 28rpx;
	color: #333333;
	font-family: Helvetica Neue, Helvetica, sans-serif;
}
view,
scroll-view,
swiper,
button,
input,
textarea,
label,
navigator,
image {
	box-sizing: border-box;
}
.round {
	border-radius: 5000rpx;
}
.radius {
	border-radius: 6rpx;
}
/* ==================
          图片
 ==================== */
image {
	max-width: 100%;
	display: inline-block;
	position: relative;
	z-index: 0;
}
image.loading::before {
	content: "";
	background-color: #f5f5f5;
	display: block;
	position: absolute;
	width: 100%;
	height: 100%;
	z-index: -2;
}
image.loading::after {
	content: "\e7f1";
	font-family: "cuIcon";
	position: absolute;
	top: 0;
	left: 0;
	width: 32rpx;
	height: 32rpx;
	line-height: 32rpx;
	right: 0;
	bottom: 0;
	z-index: -1;
	font-size: 32rpx;
	margin: auto;
	color: #ccc;
	-webkit-animation: cuIcon-spin 2s infinite linear;
	animation: cuIcon-spin 2s infinite linear;
	display: block;
}
.response {
	width: 100%;
}
/* ==================
          操作条
 ==================== */
.cu-bar {
	display: flex;
	position: relative;
	align-items: center;
	min-height: 100rpx;
	justify-content: space-between;
}
.cu-bar .action {
	display: flex;
	align-items: center;
	height: 100%;
	justify-content: center;
	max-width: 100%;
}
.cu-bar .action.border-title {
	position: relative;
	top: -10rpx;
}
.cu-bar .action.border-title text[class*="bg-"]:last-child {
	position: absolute;
	bottom: -0.5rem;
	min-width: 2rem;
	height: 6rpx;
	left: 0;
}
.cu-bar .action.sub-title {
	position: relative;
	top: -0.2rem;
}
.cu-bar .action.sub-title text {
	position: relative;
	z-index: 1;
}
.cu-bar .action.sub-title text[class*="bg-"]:last-child {
	position: absolute;
	display: inline-block;
	bottom: -0.2rem;
	border-radius: 6rpx;
	width: 100%;
	height: 0.6rem;
	left: 0.6rem;
	opacity: 0.3;
	z-index: 0;
}
.cu-bar .action.sub-title text[class*="text-"]:last-child {
	position: absolute;
	display: inline-block;
	bottom: -0.7rem;
	left: 0.5rem;
	opacity: 0.2;
	z-index: 0;
	text-align: right;
	font-weight: 900;
	font-size: 36rpx;
}
.cu-bar.justify-center .action.border-title text:last-child,
.cu-bar.justify-center .action.sub-title text:last-child {
	left: 0;
	right: 0;
	margin: auto;
	text-align: center;
}
.cu-bar .action:first-child {
	margin-left: 30rpx;
	font-size: 30rpx;
}
.cu-bar .action text.text-cut {
	text-align: left;
	width: 100%;
}
.cu-bar .cu-avatar:first-child {
	margin-left: 20rpx;
}
.cu-bar .action:first-child>text[class*="cuIcon-"] {
	margin-left: -0.3em;
	margin-right: 0.3em;
}
.cu-bar .action:last-child {
	margin-right: 30rpx;
}
.cu-bar .action>text[class*="cuIcon-"],
.cu-bar .action>view[class*="cuIcon-"] {
	font-size: 36rpx;
}
.cu-bar .action>text[class*="cuIcon-"]+text[class*="cuIcon-"] {
	margin-left: 0.5em;
}
.cu-bar .content {
	position: absolute;
	text-align: center;
	width: calc(100% - 340rpx);
	left: 0;
	right: 0;
	bottom: 0;
	top: 0;
	margin: auto;
	height: 60rpx;
	font-size: 32rpx;
	line-height: 60rpx;
	cursor: none;
	pointer-events: none;
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
}
.cu-bar.ios .content {
	bottom: 7px;
	height: 30px;
	font-size: 32rpx;
	line-height: 30px;
}
.cu-bar.btn-group {
	justify-content: space-around;
}
.cu-bar.btn-group button {
	padding: 20rpx 32rpx;
}
.cu-bar.btn-group button {
	flex: 1;
	margin: 0 20rpx;
	max-width: 50%;
}
.cu-bar .search-form {
	background-color: #f5f5f5;
	line-height: 64rpx;
	height: 64rpx;
	font-size: 24rpx;
	color: #333333;
	flex: 1;
	display: flex;
	align-items: center;
	margin: 0 30rpx;
}
.cu-bar .search-form+.action {
	margin-right: 30rpx;
}
.cu-bar .search-form input {
	flex: 1;
	padding-right: 30rpx;
	height: 64rpx;
	line-height: 64rpx;
	font-size: 26rpx;
	background-color: transparent;
}
.cu-bar .search-form [class*="cuIcon-"] {
	margin: 0 0.5em 0 0.8em;
}
.cu-bar .search-form [class*="cuIcon-"]::before {
	top: 0rpx;
}
.cu-bar.fixed,
.nav.fixed {
	position: fixed;
	width: 100%;
	top: 0;
	z-index: 1024;
	box-shadow: 0 1rpx 6rpx rgba(0, 0, 0, 0.1);
}
.cu-bar.foot {
	position: fixed;
	width: 100%;
	bottom: 0;
	z-index: 1024;
	box-shadow: 0 -1rpx 6rpx rgba(0, 0, 0, 0.1);
}
.cu-bar.tabbar {
	padding: 0;
	height: calc(100rpx + env(safe-area-inset-bottom) / 2);
	padding-bottom: calc(env(safe-area-inset-bottom) / 2);
}
.cu-tabbar-height {
	min-height: 100rpx;
	height: calc(100rpx + env(safe-area-inset-bottom) / 2);
}
.cu-bar.tabbar.shadow {
	box-shadow: 0 -1rpx 6rpx rgba(0, 0, 0, 0.1);
}
.cu-bar.tabbar .action {
	font-size: 22rpx;
	position: relative;
	flex: 1;
	text-align: center;
	padding: 0;
	display: block;
	height: auto;
	line-height: 1;
	margin: 0;
	background-color: inherit;
	overflow: initial;
}
.cu-bar.tabbar.shop .action {
	width: 140rpx;
	flex: initial;
}
.cu-bar.tabbar .action.add-action {
	position: relative;
	z-index: 2;
	padding-top: 50rpx;
}
.cu-bar.tabbar .action.add-action [class*="cuIcon-"] {
	position: absolute;
	width: 70rpx;
	z-index: 2;
	height: 70rpx;
	border-radius: 50%;
	line-height: 70rpx;
	font-size: 50rpx;
	top: -35rpx;
	left: 0;
	right: 0;
	margin: auto;
	padding: 0;
}
.cu-bar.tabbar .action.add-action::after {
	content: "";
	position: absolute;
	width: 100rpx;
	height: 100rpx;
	top: -50rpx;
	left: 0;
	right: 0;
	margin: auto;
	box-shadow: 0 -3rpx 8rpx rgba(0, 0, 0, 0.08);
	border-radius: 50rpx;
	background-color: inherit;
	z-index: 0;
}
.cu-bar.tabbar .action.add-action::before {
	content: "";
	position: absolute;
	width: 100rpx;
	height: 30rpx;
	bottom: 30rpx;
	left: 0;
	right: 0;
	margin: auto;
	background-color: inherit;
	z-index: 1;
}
.cu-bar.tabbar .btn-group {
	flex: 1;
	display: flex;
	justify-content: space-around;
	align-items: center;
	padding: 0 10rpx;
}
.cu-bar.tabbar button.action::after {
	border: 0;
}
.cu-bar.tabbar .action [class*="cuIcon-"] {
	width: 100rpx;
	position: relative;
	display: block;
	height: auto;
	margin: 0 auto 10rpx;
	text-align: center;
	font-size: 40rpx;
}
.cu-bar.tabbar .action .cuIcon-cu-image {
	margin: 0 auto;
}
.cu-bar.tabbar .action .cuIcon-cu-image image {
	width: 50rpx;
	height: 50rpx;
	display: inline-block;
}
.cu-bar.tabbar .submit {
	align-items: center;
	display: flex;
	justify-content: center;
	text-align: center;
	position: relative;
	flex: 2;
	align-self: stretch;
}
.cu-bar.tabbar .submit:last-child {
	flex: 2.6;
}
.cu-bar.tabbar .submit+.submit {
	flex: 2;
}
.cu-bar.tabbar.border .action::before {
	content: " ";
	width: 200%;
	height: 200%;
	position: absolute;
	top: 0;
	left: 0;
	-webkit-transform: scale(0.5);
	        transform: scale(0.5);
	-webkit-transform-origin: 0 0;
	        transform-origin: 0 0;
	border-right: 1rpx solid rgba(0, 0, 0, 0.1);
	z-index: 3;
}
.cu-bar.tabbar.border .action:last-child:before {
	display: none;
}
.cu-bar.input {
	padding-right: 20rpx;
	background-color: #ffffff;
}
.cu-bar.input input {
	overflow: initial;
	line-height: 64rpx;
	height: 64rpx;
	min-height: 64rpx;
	flex: 1;
	font-size: 30rpx;
	margin: 0 20rpx;
}
.cu-bar.input .action {
	margin-left: 20rpx;
}
.cu-bar.input .action [class*="cuIcon-"] {
	font-size: 48rpx;
}
.cu-bar.input input+.action {
	margin-right: 20rpx;
	margin-left: 0rpx;
}
.cu-bar.input .action:first-child [class*="cuIcon-"] {
	margin-left: 0rpx;
}
.cu-custom {
	display: block;
	position: relative;
}
.cu-custom .cu-bar .content {
	width: calc(100% - 440rpx);
}
.cu-custom .cu-bar .content image {
	height: 60rpx;
	width: 240rpx;
}
.cu-custom .cu-bar {
	min-height: 0px;

	padding-right: 220rpx;




	box-shadow: 0rpx 0rpx 0rpx;
	z-index: 9999;
}
.cu-custom .cu-bar .border-custom {
	position: relative;
	background: rgba(0, 0, 0, 0.15);
	border-radius: 1000rpx;
	height: 30px;
}
.cu-custom .cu-bar .border-custom::after {
	content: " ";
	width: 200%;
	height: 200%;
	position: absolute;
	top: 0;
	left: 0;
	border-radius: inherit;
	-webkit-transform: scale(0.5);
	        transform: scale(0.5);
	-webkit-transform-origin: 0 0;
	        transform-origin: 0 0;
	pointer-events: none;
	box-sizing: border-box;
	border: 1rpx solid #ffffff;
	opacity: 0.5;
}
.cu-custom .cu-bar .border-custom::before {
	content: " ";
	width: 1rpx;
	height: 110%;
	position: absolute;
	top: 22.5%;
	left: 0;
	right: 0;
	margin: auto;
	-webkit-transform: scale(0.5);
	        transform: scale(0.5);
	-webkit-transform-origin: 0 0;
	        transform-origin: 0 0;
	pointer-events: none;
	box-sizing: border-box;
	opacity: 0.6;
	background-color: #ffffff;
}
.cu-custom .cu-bar .border-custom text {
	display: block;
	flex: 1;
	margin: auto !important;
	text-align: center;
	font-size: 34rpx;
}
/* ==================
         导航栏
 ==================== */
.nav {
	white-space: nowrap;
}
::-webkit-scrollbar {
	display: none;
}
.nav .cu-item {
	height: 90rpx;
	display: inline-block;
	line-height: 90rpx;
	margin: 0 10rpx;
	padding: 0 20rpx;
}
.nav .cu-item.cur {
	border-bottom: 4rpx solid;
}
/* ==================
          布局
 ==================== */
/*  -- flex弹性布局 -- */
.flex {
	display: flex;
}
.flex-sub {
	flex: 1;
}
.flex-twice {
	flex: 2;
}
.flex-treble {
	flex: 3;
}
.flex-direction {
	flex-direction: column;
}
.flex-wrap {
	flex-wrap: wrap;
}
.align-start {
	align-items: flex-start;
}
.align-end {
	align-items: flex-end;
}
.align-center {
	align-items: center;
}
.align-stretch {
	align-items: stretch;
}
.self-start {
	align-self: flex-start;
}
.self-center {
	align-self: flex-center;
}
.self-end {
	align-self: flex-end;
}
.self-stretch {
	align-self: stretch;
}
.align-stretch {
	align-items: stretch;
}
.justify-start {
	justify-content: flex-start;
}
.justify-end {
	justify-content: flex-end;
}
.justify-center {
	justify-content: center;
}
.justify-between {
	justify-content: space-between;
}
.justify-around {
	justify-content: space-around;
}
/* grid布局 */
.grid {
	display: flex;
	flex-wrap: wrap;
}
.grid.grid-square {
	overflow: hidden;
}
.grid.grid-square .cu-tag {
	position: absolute;
	right: 0;
	top: 0;
	border-bottom-left-radius: 6rpx;
	padding: 6rpx 12rpx;
	height: auto;
	background-color: rgba(0, 0, 0, 0.5);
}
.grid.grid-square>view>text[class*="cuIcon-"] {
	font-size: 52rpx;
	position: absolute;
	color: #8799a3;
	margin: auto;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
}
.grid.grid-square>view {
	margin-right: 20rpx;
	margin-bottom: 20rpx;
	border-radius: 6rpx;
	position: relative;
	overflow: hidden;
}
.grid.grid-square>view.bg-img image {
	width: 100%;
	height: 100%;
	position: absolute;
}
.grid.col-1.grid-square>view {
	padding-bottom: 100%;
	height: 0;
	margin-right: 0;
}
.grid.col-2.grid-square>view {
	padding-bottom: calc((100% - 20rpx)/2);
	height: 0;
	width: calc((100% - 20rpx)/2);
}
.grid.col-3.grid-square>view {
	padding-bottom: calc((100% - 40rpx)/3);
	height: 0;
	width: calc((100% - 40rpx)/3);
}
.grid.col-4.grid-square>view {
	padding-bottom: calc((100% - 60rpx)/4);
	height: 0;
	width: calc((100% - 60rpx)/4);
}
.grid.col-5.grid-square>view {
	padding-bottom: calc((100% - 80rpx)/5);
	height: 0;
	width: calc((100% - 80rpx)/5);
}
.grid.col-2.grid-square>view:nth-child(2n),
.grid.col-3.grid-square>view:nth-child(3n),
.grid.col-4.grid-square>view:nth-child(4n),
.grid.col-5.grid-square>view:nth-child(5n) {
	margin-right: 0;
}
.grid.col-1>view {
	width: 100%;
}
.grid.col-2>view {
	width: 50%;
}
.grid.col-3>view {
	width: 33.33%;
}
.grid.col-4>view {
	width: 25%;
}
.grid.col-5>view {
	width: 20%;
}
/*  -- 内外边距 -- */
.margin-0 {
	margin: 0;
}
.margin-xs {
	margin: 10rpx;
}
.margin-sm {
	margin: 20rpx;
}
.margin {
	margin: 30rpx;
}
.margin-lg {
	margin: 40rpx;
}
.margin-xl {
	margin: 50rpx;
}
.margin-top-xs {
	margin-top: 10rpx;
}
.margin-top-sm {
	margin-top: 20rpx;
}
.margin-top {
	margin-top: 30rpx;
}
.margin-top-lg {
	margin-top: 40rpx;
}
.margin-top-xl {
	margin-top: 50rpx;
}
.margin-right-xs {
	margin-right: 10rpx;
}
.margin-right-sm {
	margin-right: 20rpx;
}
.margin-right {
	margin-right: 30rpx;
}
.margin-right-lg {
	margin-right: 40rpx;
}
.margin-right-xl {
	margin-right: 50rpx;
}
.margin-bottom-xs {
	margin-bottom: 10rpx;
}
.margin-bottom-sm {
	margin-bottom: 20rpx;
}
.margin-bottom {
	margin-bottom: 30rpx;
}
.margin-bottom-lg {
	margin-bottom: 40rpx;
}
.margin-bottom-xl {
	margin-bottom: 50rpx;
}
.margin-left-xs {
	margin-left: 10rpx;
}
.margin-left-sm {
	margin-left: 20rpx;
}
.margin-left {
	margin-left: 30rpx;
}
.margin-left-lg {
	margin-left: 40rpx;
}
.margin-left-xl {
	margin-left: 50rpx;
}
.margin-lr-xs {
	margin-left: 10rpx;
	margin-right: 10rpx;
}
.margin-lr-sm {
	margin-left: 20rpx;
	margin-right: 20rpx;
}
.margin-lr {
	margin-left: 30rpx;
	margin-right: 30rpx;
}
.margin-lr-lg {
	margin-left: 40rpx;
	margin-right: 40rpx;
}
.margin-lr-xl {
	margin-left: 50rpx;
	margin-right: 50rpx;
}
.margin-tb-xs {
	margin-top: 10rpx;
	margin-bottom: 10rpx;
}
.margin-tb-sm {
	margin-top: 20rpx;
	margin-bottom: 20rpx;
}
.margin-tb {
	margin-top: 30rpx;
	margin-bottom: 30rpx;
}
.margin-tb-lg {
	margin-top: 40rpx;
	margin-bottom: 40rpx;
}
.margin-tb-xl {
	margin-top: 50rpx;
	margin-bottom: 50rpx;
}
.padding-0 {
	padding: 0;
}
.padding-xs {
	padding: 10rpx;
}
.padding-sm {
	padding: 20rpx;
}
.padding {
	padding: 30rpx;
}
.padding-lg {
	padding: 40rpx;
}
.padding-xl {
	padding: 50rpx;
}
.padding-top-xs {
	padding-top: 10rpx;
}
.padding-top-sm {
	padding-top: 20rpx;
}
.padding-top {
	padding-top: 30rpx;
}
.padding-top-lg {
	padding-top: 40rpx;
}
.padding-top-xl {
	padding-top: 50rpx;
}
.padding-right-xs {
	padding-right: 10rpx;
}
.padding-right-sm {
	padding-right: 20rpx;
}
.padding-right {
	padding-right: 30rpx;
}
.padding-right-lg {
	padding-right: 40rpx;
}
.padding-right-xl {
	padding-right: 50rpx;
}
.padding-bottom-xs {
	padding-bottom: 10rpx;
}
.padding-bottom-sm {
	padding-bottom: 20rpx;
}
.padding-bottom {
	padding-bottom: 30rpx;
}
.padding-bottom-lg {
	padding-bottom: 40rpx;
}
.padding-bottom-xl {
	padding-bottom: 50rpx;
}
.padding-left-xs {
	padding-left: 10rpx;
}
.padding-left-sm {
	padding-left: 20rpx;
}
.padding-left {
	padding-left: 30rpx;
}
.padding-left-lg {
	padding-left: 40rpx;
}
.padding-left-xl {
	padding-left: 50rpx;
}
.padding-lr-xs {
	padding-left: 10rpx;
	padding-right: 10rpx;
}
.padding-lr-sm {
	padding-left: 20rpx;
	padding-right: 20rpx;
}
.padding-lr {
	padding-left: 30rpx;
	padding-right: 30rpx;
}
.padding-lr-lg {
	padding-left: 40rpx;
	padding-right: 40rpx;
}
.padding-lr-xl {
	padding-left: 50rpx;
	padding-right: 50rpx;
}
.padding-tb-xs {
	padding-top: 10rpx;
	padding-bottom: 10rpx;
}
.padding-tb-sm {
	padding-top: 20rpx;
	padding-bottom: 20rpx;
}
.padding-tb {
	padding-top: 30rpx;
	padding-bottom: 30rpx;
}
.padding-tb-lg {
	padding-top: 40rpx;
	padding-bottom: 40rpx;
}
.padding-tb-xl {
	padding-top: 50rpx;
	padding-bottom: 50rpx;
}
/* -- 浮动 --  */
.cf::after,
.cf::before {
	content: " ";
	display: table;
}
.cf::after {
	clear: both;
}
.fl {
	float: left;
}
.fr {
	float: right;
}
/* ==================
          背景
 ==================== */
.bg-red {
	background-color: #e54d42;
	color: #ffffff;
}
.bg-orange {
	background-color: #f37b1d;
	color: #ffffff;
}
.bg-yellow {
	background-color: #fbbd08;
	color: #333333;
}
.bg-olive {
	background-color: #8dc63f;
	color: #ffffff;
}
.bg-green {
	background-color: #39b54a;
	color: #ffffff;
}
.bg-cyan {
	background-color: #1cbbb4;
	color: #ffffff;
}
.bg-blue {
	background-color: #0081ff;
	color: #ffffff;
}
.bg-purple {
	background-color: #6739b6;
	color: #ffffff;
}
.bg-mauve {
	background-color: #9c26b0;
	color: #ffffff;
}
.bg-pink {
	background-color: #e03997;
	color: #ffffff;
}
.bg-brown {
	background-color: #a5673f;
	color: #ffffff;
}
.bg-grey {
	background-color: #8799a3;
	color: #ffffff;
}
.bg-gray {
	background-color: #f0f0f0;
	color: #333333;
}
.bg-black {
	background-color: #333333;
	color: #ffffff;
}
.bg-white {
	background-color: #ffffff;
	color: #666666;
}
.bg-shadeTop {
	background-image: linear-gradient(rgba(0, 0, 0, 1), rgba(0, 0, 0, 0.01));
	color: #ffffff;
}
.bg-shadeBottom {
	background-image: linear-gradient(rgba(0, 0, 0, 0.01), rgba(0, 0, 0, 1));
	color: #ffffff;
}
.bg-red.light {
	color: #e54d42;
	background-color: #fadbd9;
}
.bg-orange.light {
	color: #f37b1d;
	background-color: #fde6d2;
}
.bg-yellow.light {
	color: #fbbd08;
	background-color: #fef2ced2;
}
.bg-olive.light {
	color: #8dc63f;
	background-color: #e8f4d9;
}
.bg-green.light {
	color: #39b54a;
	background-color: #d7f0dbff;
}
.bg-cyan.light {
	color: #1cbbb4;
	background-color: #d2f1f0;
}
.bg-blue.light {
	color: #0081ff;
	background-color: #cce6ff;
}
.bg-purple.light {
	color: #6739b6;
	background-color: #e1d7f0;
}
.bg-mauve.light {
	color: #9c26b0;
	background-color: #ebd4ef;
}
.bg-pink.light {
	color: #e03997;
	background-color: #f9d7ea;
}
.bg-brown.light {
	color: #a5673f;
	background-color: #ede1d9;
}
.bg-grey.light {
	color: #8799a3;
	background-color: #e7ebed;
}
.bg-gradual-red {
	background-image: linear-gradient(45deg, #f43f3b, #ec008c);
	color: #ffffff;
}
.bg-gradual-orange {
	background-image: linear-gradient(45deg, #ff9700, #ed1c24);
	color: #ffffff;
}
.bg-gradual-green {
	background-image: linear-gradient(45deg, #39b54a, #8dc63f);
	color: #ffffff;
}
.bg-gradual-purple {
	background-image: linear-gradient(45deg, #9000ff, #5e00ff);
	color: #ffffff;
}
.bg-gradual-pink {
	background-image: linear-gradient(45deg, #ec008c, #6739b6);
	color: #ffffff;
}
.bg-gradual-blue {
	background-image: linear-gradient(45deg, #0081ff, #1cbbb4);
	color: #ffffff;
}
.shadow[class*="-red"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(204, 69, 59, 0.2);
}
.shadow[class*="-orange"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(217, 109, 26, 0.2);
}
.shadow[class*="-yellow"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(224, 170, 7, 0.2);
}
.shadow[class*="-olive"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(124, 173, 55, 0.2);
}
.shadow[class*="-green"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(48, 156, 63, 0.2);
}
.shadow[class*="-cyan"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(28, 187, 180, 0.2);
}
.shadow[class*="-blue"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(0, 102, 204, 0.2);
}
.shadow[class*="-purple"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(88, 48, 156, 0.2);
}
.shadow[class*="-mauve"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(133, 33, 150, 0.2);
}
.shadow[class*="-pink"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(199, 50, 134, 0.2);
}
.shadow[class*="-brown"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(140, 88, 53, 0.2);
}
.shadow[class*="-grey"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(114, 130, 138, 0.2);
}
.shadow[class*="-gray"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(114, 130, 138, 0.2);
}
.shadow[class*="-black"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(26, 26, 26, 0.2);
}
.shadow[class*="-white"] {
	box-shadow: 6rpx 6rpx 8rpx rgba(26, 26, 26, 0.2);
}
.text-shadow[class*="-red"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(204, 69, 59, 0.2);
}
.text-shadow[class*="-orange"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(217, 109, 26, 0.2);
}
.text-shadow[class*="-yellow"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(224, 170, 7, 0.2);
}
.text-shadow[class*="-olive"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(124, 173, 55, 0.2);
}
.text-shadow[class*="-green"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(48, 156, 63, 0.2);
}
.text-shadow[class*="-cyan"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(28, 187, 180, 0.2);
}
.text-shadow[class*="-blue"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(0, 102, 204, 0.2);
}
.text-shadow[class*="-purple"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(88, 48, 156, 0.2);
}
.text-shadow[class*="-mauve"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(133, 33, 150, 0.2);
}
.text-shadow[class*="-pink"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(199, 50, 134, 0.2);
}
.text-shadow[class*="-brown"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(140, 88, 53, 0.2);
}
.text-shadow[class*="-grey"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(114, 130, 138, 0.2);
}
.text-shadow[class*="-gray"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(114, 130, 138, 0.2);
}
.text-shadow[class*="-black"] {
	text-shadow: 6rpx 6rpx 8rpx rgba(26, 26, 26, 0.2);
}
.bg-img {
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
}
.bg-mask {
	background-color: #333333;
	position: relative;
}
.bg-mask::after {
	content: "";
	border-radius: inherit;
	width: 100%;
	height: 100%;
	display: block;
	background-color: rgba(0, 0, 0, 0.4);
	position: absolute;
	left: 0;
	right: 0;
	bottom: 0;
	top: 0;
}
.bg-mask view,
.bg-mask cover-view {
	z-index: 5;
	position: relative;
}
.bg-video {
	position: relative;
}
.bg-video video {
	display: block;
	height: 100%;
	width: 100%;
	object-fit: cover;
	position: absolute;
	top: 0;
	z-index: 0;
	pointer-events: none;
}
/* ==================
          文本
 ==================== */
.text-xs {
	font-size: 20rpx;
}
.text-sm {
	font-size: 24rpx;
}
.text-df {
	font-size: 28rpx;
}
.text-lg {
	font-size: 32rpx;
}
.text-xl {
	font-size: 36rpx;
}
.text-xxl {
	font-size: 44rpx;
}
.text-sl {
	font-size: 80rpx;
}
.text-xsl {
	font-size: 120rpx;
}
.text-Abc {
	text-transform: Capitalize;
}
.text-ABC {
	text-transform: Uppercase;
}
.text-abc {
	text-transform: Lowercase;
}
.text-price::before {
	content: "¥";
	font-size: 80%;
	margin-right: 4rpx;
}
.text-cut {
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
}
.text-bold {
	font-weight: bold;
}
.text-center {
	text-align: center;
}
.text-content {
	line-height: 1.6;
}
.text-left {
	text-align: left;
}
.text-right {
	text-align: right;
}
.text-red,
.line-red,
.lines-red {
	color: #e54d42;
}
.text-orange,
.line-orange,
.lines-orange {
	color: #f37b1d;
}
.text-yellow,
.line-yellow,
.lines-yellow {
	color: #fbbd08;
}
.text-olive,
.line-olive,
.lines-olive {
	color: #8dc63f;
}
.text-green,
.line-green,
.lines-green {
	color: #39b54a;
}
.text-cyan,
.line-cyan,
.lines-cyan {
	color: #1cbbb4;
}
.text-blue,
.line-blue,
.lines-blue {
	color: #0081ff;
}
.text-purple,
.line-purple,
.lines-purple {
	color: #6739b6;
}
.text-mauve,
.line-mauve,
.lines-mauve {
	color: #9c26b0;
}
.text-pink,
.line-pink,
.lines-pink {
	color: #e03997;
}
.text-brown,
.line-brown,
.lines-brown {
	color: #a5673f;
}
.text-grey,
.line-grey,
.lines-grey {
	color: #8799a3;
}
.text-gray,
.line-gray,
.lines-gray {
	color: #aaaaaa;
}
.text-black,
.line-black,
.lines-black {
	color: #333333;
}
.text-white,
.line-white,
.lines-white {
	color: #ffffff;
}
.fs-40 {
  font-size: 40rpx !important;
}
.fs-35 {
  font-size: 35rpx !important;
}
.fs-30 {
  font-size: 30rpx !important;
}
.fs-28 {
  font-size: 28rpx !important;
}
.fs-26 {
  font-size: 26rpx !important;
}
.fs-24 {
  font-size: 24rpx !important;
}
.fs-22 {
  font-size: 22rpx !important;
}
.fs-20 {
  font-size: 20rpx !important;
}
.fs-18 {
  font-size: 18rpx !important;
}
.fs-16 {
  font-size: 16rpx !important;
}
.fs-14 {
  font-size: 14rpx !important;
}
.color-111 {
  color: #111 !important;
}
.color-9292 {
  color: #929292 !important;
}
.color-989898 {
  color: #989898 !important;
}
.color-yellow {
  color: #ffe32a !important;
}
.bg-fff {
  background-color: #fff !important;
}
.icon-canju {
  font-size: 60rpx;
}

