import App from './App'
import store from '@/store/index.js'
// #ifndef VUE3
import Vue from 'vue'
// 引入vView组件库
import uView from '@/uni_modules/uview-ui/index.js'
Vue.use(uView)
// 挂载全局组件
import NavCustom from '@/components/nav-costom.vue'
Vue.component('nav-custom',NavCustom)
import HomeTitle from '@/components/hemo-title.vue'
Vue.component('home-title',HomeTitle)
import GoodItme from '@/components/good-item.vue'
Vue.component('good-item',GoodItme)
import GoodsNav from '@/components/goods-nav.vue'
Vue.component('goods-nav',GoodsNav)
import TabBar from '@/components/tab-bar.vue'
Vue.component('tab-bar',TabBar)
import CartOverlay from '@/components/cart-overlay.vue'
Vue.component('cart-overlay',CartOverlay)
import LoginOverlay from '@/components/login-overlay.vue'
Vue.component('login-overlay',LoginOverlay)



// 挂载全局异步请求方法
	import * as request from '@/utils/request.js'
	for (let key in request) {
		Vue.prototype[key] = request[key]
	}
	
	import {$loadData} from '@/utils/loadData.js'
	Vue.prototype.$loadData = $loadData
	import {$loadOrder} from '@/utils/loadOrder.js'
	Vue.prototype.$loadOrder = $loadOrder
	

Vue.config.productionTip = false

App.mpType = 'app'
const app = new Vue({
    ...App,
		store //注入状态机 
})
app.$mount()

// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif


